module SPItoAsync
	#(
		parameter CLIENT_ADDR_WIDTH = 16,
		parameter CLIENT_DATA_WIDTH = 32
	)
	(
		input iCLK,
		input iSPI_CSn,
		input iSPI_CLK,
		input iSPI_MOSI,
		output oSPI_MISO,
		output oClient_RDn,
		output oClient_WRn,
		output [CLIENT_ADDR_WIDTH - 1:0] oClient_Address,
		output [CLIENT_DATA_WIDTH - 1:0] oClient_DataIn,
		input [CLIENT_DATA_WIDTH - 1:0] iClient_DataOut
	);

	localparam [1:0] IDLE = 0,AWAKE = 1, READ = 2, WRITE = 3;

	//reg record state
	reg [1:0] m_State = IDLE;
	reg [1:0] m_Next_State;

	//reg connect to output
	reg [ CLIENT_ADDR_WIDTH - 1:0 ] m_Client_Address = 0;
	reg [ CLIENT_ADDR_WIDTH - 1:0 ] m_Next_Client_Address = 0;
	reg [ CLIENT_DATA_WIDTH - 1:0 ] m_Client_DataIn = 0;
	reg [ CLIENT_DATA_WIDTH - 1:0 ] m_Next_Client_DataIn;
	reg m_Client_WRn = 0;
	reg m_Next_Client_WRn = 0;
	reg m_Client_RDn = 0;
	reg m_Next_Client_RDn = 0;
	reg m_SPI_MISO = 0;
	reg m_Next_SPI_MISO = 0;

	assign oClient_WRn = m_Client_WRn;
	assign oClient_Address = m_Client_Address;
	assign oClient_DataIn = m_Client_DataIn;
	assign oClient_RDn = m_Client_RDn;
	assign oSPI_MISO = m_SPI_MISO;

	// reg store input information
	reg [ 23:0 ] m_Addr_Comment = { 24'd0 };
	reg [ 23:0 ] m_Next_Addr_Comment = { 24'd0 };
	reg [ CLIENT_DATA_WIDTH - 1:0 ] m_Data = { 32'd0 };
	reg [ CLIENT_DATA_WIDTH - 1:0 ] m_Next_Data = { 32'd0 };
	reg m_SPI_CSn = 1;
	reg m_Next_SPI_CSn;
	reg m_SPI_CLK = 1;
	reg m_Next_SPI_CLK;

	// reg related to Combination Logics
	reg [ 7:0 ] m_Index = { 8'd23 };
	reg [ 7:0 ] m_Next_Index;
	reg m_WRn_Already = 0;
	reg m_Next_WRn_Already;
	reg m_Finish_Write = 0;
	reg m_Next_Finish_Write;
	reg m_Finish_Read = 0;
	reg m_Next_Finish_Read;

	always @( posedge iCLK ) begin
		// update next state
		m_State <= m_Next_State;
		//update reg that store input
		m_SPI_CSn <= m_Next_SPI_CSn;
		m_SPI_CLK <= m_Next_SPI_CLK;
		m_Addr_Comment <= m_Next_Addr_Comment;
		m_Data <= m_Next_Data;
		//update logical reg update
		m_Index <= m_Next_Index;
		m_WRn_Already <= m_Next_WRn_Already;
		m_Finish_Write <= m_Next_Finish_Write;
		m_Finish_Read <= m_Next_Finish_Read;
		//update output
		m_Client_WRn <= m_Next_Client_WRn;
		m_Client_RDn <= m_Next_Client_RDn;
		m_Client_Address <= m_Next_Client_Address;
		m_Client_DataIn <= m_Next_Client_DataIn;
		m_SPI_MISO <= m_Next_SPI_MISO;
	end

	//Combination logical
	always @* begin
		m_Next_SPI_CSn = iSPI_CSn;
		m_Next_SPI_CLK = iSPI_CLK;
		//preset
		m_Next_Addr_Comment = m_Addr_Comment;
		m_Next_Client_Address = m_Client_Address;
		m_Next_Client_DataIn = m_Client_DataIn;
		m_Next_Client_RDn = 0;
		m_Next_Client_WRn = 0;
		m_Next_WRn_Already = m_WRn_Already;
		m_Next_Data = m_Data;
		m_Next_Index = m_Index;
		m_Next_SPI_MISO = 0;
		m_Next_Finish_Read = m_Finish_Read;
		m_Next_Finish_Write = m_Finish_Write;

		case( m_State )
		IDLE: begin  //IDLE
			m_Next_State = IDLE;

			//change state to AWAKE when  iSPI_CSn negedge
			if ( m_SPI_CSn == 1 && m_Next_SPI_CSn == 0 )begin
				m_Next_State = AWAKE;
			end
			//reset  reg related to logical
			m_Next_Index = { 8'd23 };
			m_Next_Finish_Write = 0;
			m_Next_Finish_Read = 0;
			//reset reg related to WEITE output
			m_Next_Client_WRn = 0;
			m_Next_Client_Address = 0;
			m_Next_Client_DataIn = 0;
			//reset reg related to READ output
			m_Next_Client_RDn = 0;
			m_Next_SPI_MISO = 0;
		end

		AWAKE: begin //AWAKE
			m_Next_State = AWAKE;

			// read Comment then change state to READ and reset  m_Index
			if( m_Addr_Comment[ 7:0 ] == { 8'b01011111 } ) begin
				m_Next_State = READ;
				//let oClient_RDn high for one iCLK tick
				m_Next_Client_RDn  = 1;
				//output oClient_Address
				m_Next_Client_Address = m_Addr_Comment[ 23:8 ];
				//reset index
				m_Next_Index = { 8'd31 };
			end
			// read Comment then change state to WRITE and reset  m_Index
			else if( m_Addr_Comment[ 7:0 ] == { 8'b10101111 } ) begin
				m_Next_State = WRITE;
				//reset index
				m_Next_Index = { 8'd31 };
			end

			//sampling iSPI_MOSI when  iSPI_CLK negedge
			if( m_SPI_CLK == 1 && m_Next_SPI_CLK == 0 ) begin
				m_Next_Addr_Comment[ m_Index ] = iSPI_MOSI ;
				m_Next_Index = m_Index - 1;
			end
		end

		READ: begin //READ
			m_Next_State = READ;

			//self-remain SPI_MOSI  before Read finish
			if( m_Finish_Read == 0 )begin
				m_Next_SPI_MISO = m_SPI_MISO;
			end

			//  change state to IDLE when  iSPI_CSn posedge
			if( m_SPI_CSn == 0 && m_Next_SPI_CSn == 1 ) begin
				m_Next_State = IDLE;
			end

			//output MISO when  iSPI_CLK negedge
			if( m_SPI_CLK == 1 && m_Next_SPI_CLK == 0 && m_Finish_Read == 0 ) begin
				m_Next_SPI_MISO = iClient_DataOut[ m_Index ];
				m_Next_Index = m_Index - 1;
			end
			//set the Read finish point at last iSPI_CLK posedge
			if( m_SPI_CLK == 0 && m_Next_SPI_CLK == 1 && m_Index == { 8'hff } )begin
				m_Next_Finish_Read = 1;
			end
		end

		WRITE: begin //WRITE
			m_Next_State = WRITE;

			//  change state to IDLE when  iSPI_CSn posedge
			if( m_SPI_CSn == 0 && m_Next_SPI_CSn == 1 ) begin
				m_Next_State = IDLE;
			end

			//sampling iSPI_MOSI  when  iSPI_CLK negedge
			if ( m_SPI_CLK == 1 && m_Next_SPI_CLK == 0 && m_Finish_Write == 0 ) begin
				m_Next_Data[ m_Index ] = iSPI_MOSI;
				m_Next_Index = m_Index - 1;
				if( m_Index == 0 ) begin
					m_Next_Finish_Write = 1;
				end
			end

			//when finish samplig then output data  and tick oClient_WRn  high
			if( m_Finish_Write == 1 )begin
				m_Next_Client_Address = m_Addr_Comment[ 23:8 ];
				m_Next_Client_DataIn  = m_Next_Data;
				if( m_WRn_Already == 0 )begin
					m_Next_Client_WRn = 1;
					m_Next_WRn_Already =1;
				end
			end

		end
		endcase
	end //end always

endmodule