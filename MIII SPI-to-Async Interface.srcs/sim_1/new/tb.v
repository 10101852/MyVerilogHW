`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2022/11/22 10:43:38
// Design Name: 
// Module Name: tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb
#(
parameter CLIENT_ADDR_WIDTH = 16,
parameter CLIENT_DATA_WIDTH = 32,
parameter TIME = 200,

parameter [CLIENT_ADDR_WIDTH - 1:0] Addr = {16'd1052},
parameter [CLIENT_DATA_WIDTH - 1:0] Data = {32'd1234},
parameter [7:0] WriteCommenct = {8'b10101111},
parameter [7:0] ReadCommenct = {8'b01011111},
parameter [55:0]EntireWrite = {Addr, WriteCommenct, Data},
parameter [55:0]EntireRead = {Addr, ReadCommenct, 32'd0}
);
reg [4:0]n;
reg [4:0]i;
reg [7:0]index = {8'd55};
// [測試訊號]
// Inputs
reg CLK;
reg SPI_CSn;
reg SPI_CLK;
reg SPI_MOSI;
reg [CLIENT_DATA_WIDTH - 1:0] Client_DataOut;

// Outputs
wire SPI_MISO;
wire Client_RDn;
wire Client_WRn;
wire [CLIENT_ADDR_WIDTH - 1:0] Client_Address;
wire [CLIENT_DATA_WIDTH - 1:0] Client_DataIn;

// [測試單元裝置]
SPItoAsync
#(
.CLIENT_ADDR_WIDTH(CLIENT_ADDR_WIDTH),
.CLIENT_DATA_WIDTH(CLIENT_DATA_WIDTH)
) 
utt(
.iCLK(CLK),
.iSPI_CSn(SPI_CSn),
.iSPI_CLK(SPI_CLK),
.iSPI_MOSI(SPI_MOSI),
.iClient_DataOut(Client_DataOut),

.oSPI_MISO(SPI_MISO),
.oClient_RDn(Client_RDn),
.oClient_WRn(Client_WRn),
.oClient_Address(Client_Address),
.oClient_DataIn(Client_DataIn)
);

always #10 CLK = ~CLK;

always@(negedge SPI_CLK)begin
	SPI_MOSI <= EntireWrite[index];
	index = index -1 ;
end


initial begin
// Initialize Inputs
i = {7'd80};
CLK = 0;
SPI_CSn = 1;
SPI_CLK = 1;
SPI_MOSI = 0;
Client_DataOut = {32'd10101852};


//start testing
#2000;
SPI_CSn = 0;
#100;
for(n=0;n<7;n=n+1)begin //7 bytes
	for(i=0;i<8;i=i+1)begin //8 bits 
		SPI_CLK = ~SPI_CLK;
		#TIME;
		SPI_CLK = ~SPI_CLK;
		#TIME;
	end
	SPI_MOSI <= 0;
	#TIME;
end

//end

#1000;
SPI_CSn = 1;

//IDLE
#1000;
end


endmodule
